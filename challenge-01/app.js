/*
* The company 'futureWD, Ltd.' has decided to get github repos of employees. Team
* has gotten the next requirements for resolving this problem:
*
* Requirements:
*
* - It is important use promises with superagent to resolve this problem. 
* - It is necessary to handle errors.
* - It's necessary to catch github username and display in browser its repos:
*
*   username --> alexisthink
*   
*   repos -->
*
*   -js-object-basics
	activity--async-fetch-and-render
	activity--dom-manipulations-master
	activity--dom-manipulations-rich-text-editor-master
	activity--dribbble
	activity--filter-rest-countries
	activity--google-search
	activity--js-functions-basic
	activity--search-movies-by-genre
	activity--youtube-thumbnails
	assignment--basic-css-web-components
	assignment--basic-html-rothko
	assignment--css-positioning
	assignment--dom-manipulation
	assignment--flexbox-layout-nike-fuelband
	assignment--js-basics
	assignment--media-query-layout-amazon-charity
	assignment--react-portfolio-01-syntax-basics
	assignment--react-portfolio-02-props
	assignment--react-portfolio-03-state
	assignment--reading-api-documentation
	assignment--sass-basics
	Bot-Alexis
	dom-manipulations-events
	dom-manipulations-inputs
	excercise-nullData
	Express-Basics
	Express-Basics-Error-500-
	Java-Clase-01
	Java-Clase-03
*
* It's important that algorithm does not accept empty values.
* Team has decided to read documentation in REST API v3 for GITHUB Developers 
* (https://developer.github.com/v3/) to know how to get the repositories of each 
* employee.
*
* Team has asked its developers to use its code structure.
*/


function getRepo(name) {

}

function getUser() {

}