/*
* The company 'XZeta, Ltd.' has used XMLHttpRequest object for making requests 
* to fetch data from API - http://sepomex.icalialabs.com/api/v1/states, however 
* the problem is too much code. Check code below.
*
*
* Requirements:
*
* - It is important use promises with superagent to resolve this problem. 
* - It is necessary to handle errors.
* - It's necessary display in browser next data:
*
*
          Name              Cities Count
    Ciudad de México            16
    Aguascalientes              11
    Baja California              5
    Baja California              5
    Campeche                    11
    Coahuila de Zaragoza        38
    Colima                      10
    Chiapas                     118
    Chihuahua                   67
    Durango                     39
    Guanajuato                  46
    Guerrero                    81
    Hidalgo                     84
    Jalisco                     125
    México                      125
    Michoacán de Ocampo         113
    Morelos                     33
    Nayarit                     20
    Nuevo León                  51
    Oaxaca                      570
    Puebla                      217
    Querétaro                   18
    Quintana Roo                10
    San Luis Potosí             58
    Sinaloa                     18
    Sonora                      72
    Tabasco                     17
    Tamaulipas                  43
    Tlaxcala                    60
    Veracruz de Ignacio de la Llave 212
    Yucatán                     106
    Zacatecas                   58

*   
*/


/* Old Way of Requesting remote data*/

// const xhr = new XMLHttpRequest();
// xhr.open('GET', 'http://sepomex.icalialabs.com/api/v1/states')
// xhr.send()

// xhr.onreadystatechange = function(){
//   if(xhr.status === 200 && xhr.responseText){
//     console.log('SUCCESS')
//     console.log(xhr.responseText)
//   }

//   if(xhr.status >= 400){
//     console.log('ERROR!!!')
//     console.log(xhr.responseText)
//   }
// }


/*  Requesting remote data */